# Steletric

Stellaris meets metrics. Steletric tracks datapoints for all empires over the course of a game. This allows viewing of, for example, the available minerals over a period of time. There are **many** different metrics to view.

![steletric banner](https://milkmanmatty.com.au/media/1435/steletric.png)

## Starting Development

Start the app in the `dev` environment:

```bash
yarn start
```

## Packaging for Production

To package apps for the local platform:

```bash
yarn run package
```

## License

MIT © [Steletric](https://bitbucket.org/milkymanmatt/stelectric)
