import '@testing-library/jest-dom';
// import path from 'path';
import sqlite3, { Statement } from 'sqlite3';
import { Database, open } from 'sqlite';
import GetGameFolder from '../../main/data/GetGameFolder';
import DatabaseConnection from '../../main/data/DatabaseConnection';
import DatabaseSetup from '../../main/data/DatabaseSetup';
import log from '../../main/services/Logger';

let db: Promise<Database<sqlite3.Database, Statement>>;

jest.mock('../../main/data/DatabaseConnection', () =>
  jest.fn().mockImplementation(() => {
    if (db) {
      return db;
    }
    db = open({
      filename: ':memory:',
      driver: sqlite3.Database,
    });
    return db;
  })
);

describe('GetGameFolder', () => {
  afterEach(() => {
    // create a new in-mem db
    db = open({
      filename: ':memory:',
      driver: sqlite3.Database,
    });
  });

  it('should create new entry when given non-existing correct data', async () => {
    log(`starting db test:`);
    await DatabaseSetup();
    expect(DatabaseConnection).toHaveBeenCalledTimes(1);

    const tables = await (
      await db
    ).all(`select name from sqlite_master where type='table'`);
    expect(tables.length).toBeGreaterThanOrEqual(1);

    const result = await GetGameFolder('this/is/a/test');
    expect(result.ID).toBe(1);
  });

  it('should return existing entry when given data that already exists', async () => {
    await DatabaseSetup();
    const tables = await (
      await db
    ).all(`select name from sqlite_master where type='table'`);
    expect(tables.length).toBeGreaterThanOrEqual(1);

    const entry1 = await GetGameFolder('this/is/a/test');
    expect(entry1.ID).toBe(1);

    const entry2 = await GetGameFolder('this/is/another/test');
    expect(entry2.ID).toBe(2);

    const entry3 = await GetGameFolder('this/test/is/last');
    expect(entry3.ID).toBe(3);

    const result = await GetGameFolder('this/is/a/test');
    expect(result.ID).toBe(1);
  });
});
