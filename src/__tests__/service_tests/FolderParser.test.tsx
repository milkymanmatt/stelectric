import '@testing-library/jest-dom';
import path from 'path';
import folderParser from '../../main/services/FolderParser';

describe('FolderParser', () => {
  it('should throw when folder path does not exist', async () => {
    // eslint-disable-next-line jest/valid-expect
    expect(folderParser('some/made/up/place')).rejects.toThrow();
  });

  it('should throw when folder path contains no sav files', async () => {
    await expect(folderParser('../')).rejects.toThrow();
  });

  it('should supply valid SnapShots when given valid folder', async () => {
    const result = await folderParser(
      `${path.resolve('./')}/test_data/validSaveFolder1`
    );

    expect(result).toBeTruthy();
  });
});
