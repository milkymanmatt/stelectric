import '@testing-library/jest-dom';
import TraditionParser from '../../../main/services/parsers/TraditionParser';
import validTraditionsInput from './TradsParser.testdata';

describe('TraditionParser', () => {
  it('should give empty tradition obj when given empty string', async () => {
    const result = TraditionParser('');
    expect(result).toBeTruthy();
    expect(result.ascPerkCount).toBe(0);
    expect(result.traditionCount).toBe(0);
    expect(result.treesUnlocked).toBe(0);
  });

  it('should pass when given valid tradition obj', async () => {
    const data = await validTraditionsInput();
    const result = TraditionParser(data);
    expect(result).toBeTruthy();
    expect(result.ascPerkCount).toBe(8);
    expect(result.traditionCount).toBe(49);
    expect(result.treesUnlocked).toBe(7);
  });
});
