/* eslint-disable jest/no-disabled-tests */
/* eslint-disable jest/expect-expect */
/* eslint-disable jest/no-export */
import fs from 'fs';

// It is an ugly workaround but it'll do pig.
test.skip('skip', () => {});

export const validCountryInput = async () => {
  const fileContents = await new Promise<string>((resolve, reject) => {
    fs.readFile('./test_data/validCountryInput.txt', 'utf8', (error, data) => {
      if (error) {
        reject(error);
      }
      if (data) {
        resolve(data);
      }
      reject(new Error('no data'));
    });
  });

  return fileContents;
};

export const invalidCountryInput = () => {
  return '';
};
