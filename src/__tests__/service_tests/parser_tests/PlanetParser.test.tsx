import '@testing-library/jest-dom';
import PlanetParser from '../../../main/services/parsers/PlanetParser';
import validPlanetInput from './PlanetParser.testdata';

describe('PlanetParser', () => {
  it('should throw when given empty string', async () => {
    const result = PlanetParser('');
    expect(result).toBeTruthy();
    expect(result.planetCount).toBe(0);
    expect(result.controlledPlanetCount).toBe(0);
  });

  it('should pass when given valid planet obj', async () => {
    const data = await validPlanetInput();
    const result = PlanetParser(data);
    expect(result).toBeTruthy();
    expect(result.planetCount).toBeGreaterThan(0);
    expect(result.controlledPlanetCount).toBeGreaterThan(0);
  });
});
