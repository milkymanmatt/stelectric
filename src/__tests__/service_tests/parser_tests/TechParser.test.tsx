import '@testing-library/jest-dom';
import TechnologyParser from '../../../main/services/parsers/TechnologyParser';
import { validTechObject } from './TechParser.testdata';

describe('TechnologyParser', () => {
  it('should give empty Tech obj when given empty string', async () => {
    const result = TechnologyParser('');
    expect(result).toBeTruthy();
    expect(result.researchedTechCount).toBe(0);
    expect(result.phyIncome).toBe(0);
    expect(result.socIncome).toBe(0);
    expect(result.engIncome).toBe(0);
    expect(result.storedPhyCount).toBe(0);
    expect(result.storedSocCount).toBe(0);
    expect(result.storedEngCount).toBe(0);
  });

  it('should pass when given valid tech obj', async () => {
    const data = await validTechObject();
    const result = TechnologyParser(data);
    expect(result).toBeTruthy();
    expect(result.researchedTechCount).toBeGreaterThanOrEqual(0);
    expect(result.phyIncome).toBeGreaterThanOrEqual(0);
    expect(result.socIncome).toBeGreaterThanOrEqual(0);
    expect(result.engIncome).toBeGreaterThanOrEqual(0);
    expect(result.storedPhyCount).toBeGreaterThanOrEqual(0);
    expect(result.storedSocCount).toBeGreaterThanOrEqual(0);
    expect(result.storedEngCount).toBeGreaterThanOrEqual(0);
  });
});
