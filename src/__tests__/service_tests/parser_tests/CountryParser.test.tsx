import '@testing-library/jest-dom';
import countryParser from '../../../main/services/parsers/CountryParser';
import { validCountryInput } from './CountryParser.testdata';

describe('CountryParser', () => {
  it('should throw when given empty string', async () => {
    await expect(countryParser('', '')).rejects.toThrow();
  });

  it('should pass when given valid filecontents', async () => {
    const data = await validCountryInput();
    const result = await countryParser(data, 'gamestate_only');

    expect(result).toBeTruthy();
    expect(result[0].name).toBe('Zanzilly Hive'); // Name does not have its own parser
  });
});
