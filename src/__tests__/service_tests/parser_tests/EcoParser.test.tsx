import '@testing-library/jest-dom';
import EconomyParser from '../../../main/services/parsers/EconomyParser';
import { validEcoAvailObject, validEcoObject } from './EcoParser.testdata';

describe('EconomyParser', () => {
  it('should give empty Eco obj when given empty string for budgetinput', async () => {
    const data2 = await validEcoAvailObject();
    const result = EconomyParser('', data2);

    expect(result).toBeTruthy();
    expect(result.energy.available).toBe(0);
    expect(result.minerals.income).toBe(0);
    expect(result.food.balance).toBe(0);
    expect(result.sr_dark_matter.available).toBe(0);
    expect(result.minor_artifacts.available).toBe(0);
  });

  it('should give empty Eco obj when given empty string for availableInput', async () => {
    const data = await validEcoObject();
    const result = EconomyParser(data, '');

    expect(result).toBeTruthy();
    expect(result.energy.available).toBe(0);
    expect(result.minerals.income).toBe(0);
    expect(result.food.balance).toBe(0);
    expect(result.sr_dark_matter.available).toBe(0);
    expect(result.minor_artifacts.available).toBe(0);
  });

  it('should pass when given valid eco objs', async () => {
    const data = await validEcoObject();
    const data2 = await validEcoAvailObject();

    const result = EconomyParser(data, data2);

    expect(result).toBeTruthy();
    expect(result.energy.available).toBe(100000);
    expect(result.minerals.income).toBe(7467.84);
    expect(result.food.balance).toBe(3458.708);
    expect(result.sr_dark_matter.available).toBe(6161.7);
    expect(result.minor_artifacts.available).toBe(121);
  });
});
