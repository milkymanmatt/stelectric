import '@testing-library/jest-dom';
import OverallParser from '../../../main/services/parsers/OverallParser';
import validOverObject from './OverParser.testdata';

describe('OverallParser', () => {
  it('should throw when given empty string', async () => {
    expect(() => OverallParser('')).toThrow();
  });

  it('should pass when given valid overall obj', async () => {
    const data = await validOverObject();
    const result = OverallParser(data);
    expect(result).toBeTruthy();
    expect(result.military_power).toBeGreaterThan(10000);
    expect(result.economy_power).toBeGreaterThan(10000);
    expect(result.tech_power).toBeGreaterThan(10000);
    expect(result.immigration).toBeGreaterThan(300);
    expect(result.fleet_size).toBeGreaterThan(1000);
    expect(result.empire_size).toBeGreaterThan(1000);
  });
});
