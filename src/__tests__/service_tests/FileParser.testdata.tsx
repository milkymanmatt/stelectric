/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable jest/expect-expect */
/* eslint-disable jest/no-disabled-tests */
/* eslint-disable jest/no-export */
import { Readable } from 'stream';
import yauzl, { Entry } from 'yauzl-promise';
import { validCountryInput } from './parser_tests/CountryParser.testdata';
import FileSections from '../../data_types/FileSections';

// It's ugly but it'll do pig.
test.skip('skip', () => {});

export const validGamestateSavFile = async () => {
  const zipFile = await yauzl.open('./test_data/gamestate_only.sav');
  const entry = await zipFile.readEntries(1);
  return entry[0];
};

export const fileChopperMockReturn = async () => {
  const result: FileSections = {
    date: '2342.06.20',
    country: await validCountryInput(),
    ships: '',
  };

  return result;
};

export const emptyEntry: Entry = {
  openReadStream(): Promise<Readable> {
    throw new Error('Function not implemented.');
  },
  comment: '',
  compressedSize: 0,
  compressionMethod: 0,
  crc32: 0,
  externalFileAttributes: 0,
  extraFieldLength: 0,
  extraFields: [],
  fileCommentLength: 0,
  fileName: '',
  fileNameLength: 0,
  generalPurposeBitFlag: 0,
  internalFileAttributes: 0,
  lastModFileDate: 0,
  lastModFileTime: 0,
  relativeOffsetOfLocalHeader: 0,
  uncompressedSize: 0,
  versionMadeBy: 0,
  versionNeededToExtract: 0,
  getLastModDate(): Date {
    throw new Error('Function not implemented.');
  },
  isEncrypted(): boolean {
    throw new Error('Function not implemented.');
  },
  isCompressed(): boolean {
    throw new Error('Function not implemented.');
  },
};

export const validEntry: Entry = {
  openReadStream(): Promise<Readable> {
    throw new Error('Function not implemented.');
  },
  comment: '',
  compressedSize: 2697736,
  compressionMethod: 8,
  crc32: 181508181,
  externalFileAttributes: 32,
  extraFieldLength: 0,
  extraFields: [
    {
      id: 10,
      data: Buffer.from([
        0, 0, 0, 0, 1, 0, 24, 0, 29, 41, 156, 156, 244, 15, 216, 1, 62, 87, 194,
        156, 244, 15, 216, 1, 48, 137, 212, 135, 244, 15, 216, 1,
      ]),
    },
  ],
  fileCommentLength: 0,
  fileName: 'gamestate',
  fileNameLength: 9,
  generalPurposeBitFlag: 0,
  internalFileAttributes: 0,
  lastModFileDate: 21559,
  lastModFileTime: 22543,
  relativeOffsetOfLocalHeader: 0,
  uncompressedSize: 34485662,
  versionMadeBy: 63,
  versionNeededToExtract: 20,
  getLastModDate(): Date {
    throw new Error('Function not implemented.');
  },
  isEncrypted(): boolean {
    throw new Error('Function not implemented.');
  },
  isCompressed(): boolean {
    throw new Error('Function not implemented.');
  },
};

export const nonGamestateValidEntry: Entry = {
  openReadStream(): Promise<Readable> {
    throw new Error('Function not implemented.');
  },
  comment: '',
  compressedSize: 2697736,
  compressionMethod: 8,
  crc32: 181508181,
  externalFileAttributes: 32,
  extraFieldLength: 0,
  extraFields: [
    {
      id: 10,
      data: Buffer.from([
        0, 0, 0, 0, 1, 0, 24, 0, 29, 41, 156, 156, 244, 15, 216, 1, 62, 87, 194,
        156, 244, 15, 216, 1, 48, 137, 212, 135, 244, 15, 216, 1,
      ]),
    },
  ],
  fileCommentLength: 0,
  fileName: 'woopsies',
  fileNameLength: 9,
  generalPurposeBitFlag: 0,
  internalFileAttributes: 0,
  lastModFileDate: 21559,
  lastModFileTime: 22543,
  relativeOffsetOfLocalHeader: 0,
  uncompressedSize: 34485662,
  versionMadeBy: 63,
  versionNeededToExtract: 20,
  getLastModDate(): Date {
    throw new Error('Function not implemented.');
  },
  isEncrypted(): boolean {
    throw new Error('Function not implemented.');
  },
  isCompressed(): boolean {
    throw new Error('Function not implemented.');
  },
};
