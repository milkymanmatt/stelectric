import '@testing-library/jest-dom';
import fileChopper from '../../main/services/FileChopper';
import { cutDownValidGamestateFile } from './FileChopper.testdata';

describe('FileChopper', () => {
  it('should throw when given empty string', async () => {
    await expect(fileChopper('', '')).rejects.toThrow();
  });

  it('should pass when given valid filecontents', async () => {
    const data = await cutDownValidGamestateFile();
    const result = await fileChopper(data, 'cutDownValidGamestateFile');

    await expect(result).toBeTruthy();
  });
});
