/* eslint-disable @typescript-eslint/no-unused-vars */
import '@testing-library/jest-dom';
import FileParser from '../../main/services/FileParser';
import {
  emptyEntry,
  fileChopperMockReturn,
  nonGamestateValidEntry,
  validEntry,
  validGamestateSavFile,
} from './FileParser.testdata';

describe('FileParser', () => {
  const mock = (_: string, __: string) => {
    return fileChopperMockReturn();
  };

  it('should throw when given Entry is empty', async () => {
    await expect(
      FileParser('../', emptyEntry, 'test_emptyEntry', mock, -1)
    ).rejects.toThrow();
  });

  it('should throw when given Entry is not named gamestate', async () => {
    await expect(
      FileParser('../', nonGamestateValidEntry, 'test_noGamestate', mock, -1)
    ).rejects.toThrow();
  });

  it('should throw when Entry is missing zipFile property', async () => {
    await expect(
      FileParser('../', validEntry, 'test_missingZipProp', mock, -1)
    ).rejects.toThrow();
  });

  it('should pass when given valid Entry', async () => {
    const result = await FileParser(
      '../',
      await validGamestateSavFile(),
      'test_gamestate_only',
      mock,
      -1
    );

    expect(result).toBeTruthy();
    expect(result.countrySnapshots.length).toBeGreaterThanOrEqual(2);
  });
});
