import '@testing-library/jest-dom';
import path from 'path';
import fileDecompressor from '../../main/services/FileDecompressor';

describe('FileDecompressor', () => {
  it('should throw when given empty param', async () => {
    await expect(fileDecompressor('')).rejects.toThrow();
  });

  it('should throw when file path does not exist', async () => {
    await expect(fileDecompressor('some/made/up/place')).rejects.toThrow();
  });

  it('should throw when file is not .sav', async () => {
    await expect(fileDecompressor('some/made/up/place.txt')).rejects.toThrow();
  });

  it('should throw when sav is empty', async () => {
    await expect(
      fileDecompressor(`${path.resolve('./')}/test_data/empty.sav`)
    ).rejects.toThrow();
  });

  it('should return gamestate when given valid sav file', async () => {
    await expect(
      fileDecompressor(`${path.resolve('./')}/test_data/validSaveFile.sav`)
    ).resolves.toBeTruthy();
  });
});
