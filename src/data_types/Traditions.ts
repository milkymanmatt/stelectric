type Traditions = {
  traditionCount: number;
  treesUnlocked: number;
  ascPerkCount: number;
};

export default Traditions;
