import { Entry as BaseEntry, ZipFile } from 'yauzl-promise';

class EntryStel extends BaseEntry {
  zipFile?: ZipFile;
}

export default EntryStel;
