type Military = {
  starbaseCount: number;
  starbaseCapacity: number;
  militaryShipCount: number;
  corvetteCount: number;
  destroyerCount: number;
  cruiserCount: number;
  battleshipCount: number;
  fleetMilitaryPower: number;
};

export default Military;
