// This is in the order that they show up under standard_economy_module in the *.sav > gamestate file
type Resource =
  | 'energy'
  | 'minerals'
  | 'food'
  | 'influence'
  | 'unity'
  | 'consumer_goods'
  | 'alloys'
  | 'volatile_motes'
  | 'exotic_gases'
  | 'rare_crystals'
  | 'sr_dark_matter'
  | 'nanites'
  | 'minor_artifacts'
  | null;

export default Resource;
