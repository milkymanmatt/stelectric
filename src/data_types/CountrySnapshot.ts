import Economy from './Economy';
import Epics from './Epics';
import Military from './Military';
import Overall from './Overall';
import Planets from './Planet';
import Population from './Population';
import Technology from './Technology';
import Traditions from './Traditions';

type CountrySnapshot = {
  name: string;
  overall: Overall;
  economy: Economy;
  population: Population;
  military: Military;
  technology: Technology;
  planets: Planets;
  traditions: Traditions;
  epics: Epics;
};

export default CountrySnapshot;
