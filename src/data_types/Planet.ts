type Planets = {
  planetCount: number;
  controlledPlanetCount: number;
  systemCount: number;
  colonyShipCount: number;
  constructionShipCount: number;
  scienceShipCount: number;
};

export default Planets;
