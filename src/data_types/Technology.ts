type Technology = {
  researchedTechCount: number;
  phyIncome: number;
  socIncome: number;
  engIncome: number;
  storedPhyCount: number;
  storedSocCount: number;
  storedEngCount: number;
};

export default Technology;
