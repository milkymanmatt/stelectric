import CountrySnapshot from './CountrySnapshot';

type FileSnapshot = {
  date: string;
  countrySnapshots: CountrySnapshot[];
};

export default FileSnapshot;
