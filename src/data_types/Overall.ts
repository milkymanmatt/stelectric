type Overall = {
  military_power: number;
  economy_power: number;
  victory_rank: number;
  victory_score: number;
  tech_power: number;
  immigration: number;
  emigration: number;
  fleet_size: number;
  empire_size: number;
  sapient: number;
};

export default Overall;
