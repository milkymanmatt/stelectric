export type Navigation = {
  buttons: NavButton[];
};

export type NavButton = {
  text: string;
  valueid: string;
  sideNavButtons: SideNavButton[];
};

export type SideNavButton = {
  text: string;
  valueid: string;
};
