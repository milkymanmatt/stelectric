type Economy = {
  energy: {
    available: number;
    income: number;
    balance: number;
  };
  minerals: {
    available: number;
    income: number;
    balance: number;
  };
  food: {
    available: number;
    income: number;
    balance: number;
  };
  consumer_goods: {
    available: number;
    income: number;
    balance: number;
  };
  alloys: {
    available: number;
    income: number;
    balance: number;
  };
  influence: {
    available: number;
    income: number;
    balance: number;
  };
  unity: {
    available: number;
    income: number;
    balance: number;
  };
  volatile_motes: {
    available: number;
    income: number;
    balance: number;
  };
  exotic_gases: {
    available: number;
    income: number;
    balance: number;
  };
  rare_crystals: {
    available: number;
    income: number;
    balance: number;
  };
  sr_dark_matter: {
    available: number;
    income: number;
    balance: number;
  };
  nanites: {
    available: number;
    income: number;
    balance: number;
  };
  minor_artifacts: {
    available: number;
  };
};

export default Economy;
