type FileSections = {
  date: string;
  country: string;
  ships: string;
};

export default FileSections;
