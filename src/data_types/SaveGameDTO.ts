import FileSnapshot from './FileSnapshot';

type SaveGameDTO = {
  ID: number;
  Folderpath: string;
  FileName: string;
  Year: string;
  SnapshotObj: FileSnapshot;
  GameFolderID: number;
};

export default SaveGameDTO;
