type Population = {
  populationCount: number;
};

export default Population;
