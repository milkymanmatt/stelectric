type GameFolderDTO = {
  ID: number;
  Folderpath: string;
  Foldername: string;
};

export default GameFolderDTO;
