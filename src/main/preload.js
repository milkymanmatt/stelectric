const { contextBridge, ipcRenderer } = require('electron');

contextBridge.exposeInMainWorld('ipc_api', {
  ipcRenderer: {
    loadNav: () => ipcRenderer.invoke('loadNav'),
    log: (toLog) => ipcRenderer.send('log', toLog),
    on(channel, func) {
      const validChannels = ['selectedFolderMenu', 'gamedata'];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.on(channel, (event, ...args) => func(...args));
      }
    },
    once(channel, func) {
      const validChannels = ['selectedFolderMenu', 'gamedata'];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.once(channel, (event, ...args) => func(...args));
      }
    },
  },
});
