/* eslint-disable @typescript-eslint/return-await */
import sqlite3, { OPEN_CREATE, OPEN_READWRITE } from 'sqlite3';
import { open } from 'sqlite';
import log from '../services/Logger';

/**
 * Is this actually a service? I'm not really sure.
 */

const DatabaseConnection = async () => {
  try {
    const db = await open({
      filename: `./db/database.db`,
      mode: OPEN_READWRITE | OPEN_CREATE,
      driver: sqlite3.Database,
    });
    return db;
  } catch (err: Error | any) {
    log(err.message);
    throw new Error('DatabaseConnection| Could not connect to DB');
  }
};

export default DatabaseConnection;
