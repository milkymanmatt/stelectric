/* eslint-disable @typescript-eslint/return-await */
import path from 'path';
import GameFolderDTO from '../../data_types/GameFolderDTO';
import log from '../services/Logger';
import DatabaseConnection from './DatabaseConnection';

/**
 * Searches for a GameFolderDTO object in the database with the same `folderpath`.
 * If one is not found it is created.
 * @param folderpath the folderpath in the db to search for
 * @param run keeps track of recursion.
 * @returns GameFolderDTO object.
 */

const GetGameFolder = async (
  folderpath: string,
  run = 0
): Promise<GameFolderDTO> => {
  if (run > 5) {
    log(
      `GetGameFolder| Attempt to get GameFolderID entered a recursive loop and failed. path: ${folderpath}`
    );
    throw new Error('Recursive loop detected, exiting out');
  }

  try {
    const db = await DatabaseConnection();
    log(`GetGameFolder| Looking for existing folder with path ${folderpath}`);
    const existing = await db.get<GameFolderDTO>(
      `SELECT * FROM GameFolder WHERE folderpath = ?`,
      folderpath
    );
    if (existing) {
      log(`GetGameFolder| GameFolderID Found`);
      return existing;
    }

    // Create new
    log(`GetGameFolder| No GameFolder entry exists, creating one.`);
    await db.run(
      `INSERT INTO GameFolder (Folderpath, FolderName) VALUES (:Folderpath, :FolderName)`,
      {
        ':Folderpath': folderpath,
        ':FolderName': path.basename(folderpath),
      }
    );
    return GetGameFolder(folderpath, run + 1);
  } catch (err: any) {
    log(`GetGameFolder| Error: ${err.message}`);
    throw err;
  }
};

export default GetGameFolder;
