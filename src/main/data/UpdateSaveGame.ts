import FileSnapshot from 'data_types/FileSnapshot';
import SaveGameDTO from '../../data_types/SaveGameDTO';
import log from '../services/Logger';
import DatabaseConnection from './DatabaseConnection';

/**
 * Updates or creates a SaveGame entity in the database.
 * @param folderpath data for SaveGame entry in the db
 * @param fileName data for SaveGame entry in the db
 * @param year data for SaveGame entry in the db
 * @param data the snapshot object for SaveGame entry in the db
 * @param gfID the GameFolderID for SaveGame entry in the db
 * @returns SaveGameDTO object containing the new or updated SaveGame entity.
 */
const UpdateSaveGame = async (
  folderpath: string,
  fileName: string,
  year: string,
  data: FileSnapshot,
  gfID: number
): Promise<SaveGameDTO> => {
  const db = await DatabaseConnection();
  log(
    `UpdateSaveGame| Looking for existing SaveGame entry with:\r\n
    ${folderpath}\r\n
    ${fileName}\r\n
    ${year} `
  );
  let existing = await db.get<SaveGameDTO>(
    `SELECT * FROM SaveGame WHERE folderpath = @1 AND FileName = @2 AND year = @3`,
    { '@1': folderpath, '@2': fileName, '@3': year }
  );
  if (existing) {
    log(`UpdateSaveGame| SaveGame Found`);
    await db.run(`UPDATE SaveGame SET SnapshotObj = @1 WHERE ID = @2`, {
      '@1': JSON.stringify(data),
      '@2': existing.ID,
    });
    log(`UpdateSaveGame| SaveGame Updated`);
    return existing;
  }
  log(`UpdateSaveGame| No SaveGame entry exists, creating one.`);
  await db.run(
    `INSERT INTO SaveGame (Folderpath, FileName, Year, SnapshotObj, GameFolderID)
    VALUES (:Folderpath, :FileName, :Year, :SnapshotObj, :GameFolderID)`,
    {
      ':Folderpath': folderpath,
      ':FileName': fileName,
      ':Year': year,
      ':SnapshotObj': JSON.stringify(data),
      ':GameFolderID': gfID,
    }
  );

  existing = await db.get<SaveGameDTO>(
    `SELECT * FROM SaveGame WHERE folderpath = @1 AND FileName = @2 AND year = @3`,
    { '@1': folderpath, '@2': fileName, '@3': year }
  );
  if (existing) {
    return existing;
  }
  log(`UpdateSaveGame| Failed to create new SaveGame entry.`);
  throw new Error(`Database failed to create new SaveGame entry.`);
};

export default UpdateSaveGame;
