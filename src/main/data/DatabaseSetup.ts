import log from '../services/Logger';
import DatabaseConnection from './DatabaseConnection';

const DatabaseSetup = async () => {
  log('Obtaining initial DB connection');
  const db = await DatabaseConnection();
  log('Check initial tables exist');
  await db.exec(`CREATE TABLE IF NOT EXISTS GameFolder (
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    Folderpath TEXT,
    FolderName TEXT
  )`);
  await db.exec(`CREATE TABLE IF NOT EXISTS SaveGame (
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    Folderpath TEXT,
    FileName TEXT,
    Year NVARCHAR(10),
    SnapshotObj TEXT,
    GameFolderID INT,
    FOREIGN KEY(GameFolderID) REFERENCES GameFolder(ID)
  )`);
  await db.exec(`CREATE TABLE IF NOT EXISTS Settings (
    KEY NVARCHAR(16) PRIMARY KEY,
    VALUE TEXT
  )`);
  log('Tables exist with certainty');
};

export default DatabaseSetup;
