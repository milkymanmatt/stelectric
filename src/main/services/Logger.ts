import jetpack from 'fs-jetpack';

let startDate = new Date();

export const logLoc = () => {
  // Update the day if it changes, who knows how long this app will be open for.
  if (new Date().getDay !== startDate.getDay) {
    startDate = new Date();
  }

  const dd = String(startDate.getDate()).padStart(2, '0');
  const mm = String(startDate.getMonth() + 1).padStart(2, '0'); // January is 0!
  const yyyy = startDate.getFullYear();
  return `./log/${yyyy}_${mm}_${dd}.log`;
};

const getTimeOfDay = () => {
  const date = new Date();
  const h = String(date.getHours());
  const m = String(date.getMinutes());
  const s = String(date.getSeconds());
  const ms = String(date.getMilliseconds());
  return `${h}:${m}:${s}.${ms}`;
};

const log = (toLog: string, timestamp = true, newline = true) => {
  let logMsg = toLog;
  if (timestamp) {
    logMsg = `[${getTimeOfDay()}] ${toLog}`;
  }
  if (newline) {
    logMsg += `\r\n`;
  }
  jetpack.append(logLoc(), logMsg);
};

export default log;
