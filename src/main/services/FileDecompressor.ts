import * as jetpack from 'fs-jetpack';
import path from 'path';
import yauzl, { Entry } from 'yauzl-promise';

export type DecompressedFile = {
  entry: Entry | null;
  filename: string;
};

const FileDecompressor = async (
  filePath: string
): Promise<DecompressedFile> => {
  if (!filePath) {
    throw new Error(`filePath is not set.`);
  }

  if (jetpack.exists(filePath) !== 'file') {
    throw new Error(`filePath ${filePath} is not a file or does not exist.`);
  }

  // Attempt to decompress/inflate passed file
  const zipFile = await yauzl.open(filePath);
  const entries = await zipFile.readEntries(2);

  if (entries.length !== 2) {
    throw new Error(
      'Sav file being unzipped contains incorrect number of files.'
    );
  }

  for (let i = 0; i < entries.length; i++) {
    if (entries[i].fileName === 'gamestate') {
      return { entry: entries[i], filename: path.parse(filePath).name };
    }
  }

  zipFile.close();
  return { entry: null, filename: path.parse(filePath).name };
};

export default FileDecompressor;
