import * as jetpack from 'fs-jetpack';
import getEmptyCountrySnapshot from './EmptyGenerators';
import FileChopper from './FileChopper';
import FileDecompressor, { DecompressedFile } from './FileDecompressor';
import FileParser from './FileParser';
import DBFolderID from '../data/GetGameFolder';
import FileSnapshot from '../../data_types/FileSnapshot';

const FolderParser = async (folderPath: string): Promise<FileSnapshot[]> => {
  if (!folderPath) {
    throw new Error('folderPath is not set');
  }

  if (!jetpack.exists(folderPath)) {
    throw new Error(`folderPath ${folderPath} does not exist.`);
  }

  const fileNames = jetpack.find(folderPath, { matching: './*.sav' });
  if (!fileNames || !fileNames.length) {
    throw new Error(`No sav files in folderPath ${folderPath}.`);
  }

  const gameFolder = await DBFolderID(folderPath);

  const allDecomFiles = await Promise.all(
    fileNames.map(async (fn) => {
      const dumb = await FileDecompressor(fn);
      return dumb;
    })
  );

  // Remove null entries
  // Bit of scary casting here
  const decomFiles: DecompressedFile[] = allDecomFiles.filter(
    (adf): adf is DecompressedFile => adf.entry !== null
  );

  const parsedFiles = await Promise.all(
    decomFiles.map(async (df) => {
      // This null check is useless and will never be used but typeScript is dumb.
      if (!df.entry) {
        return {
          date: '',
          countrySnapshots: [getEmptyCountrySnapshot()],
        };
      }
      const result = await FileParser(
        folderPath,
        df.entry,
        df.filename,
        FileChopper,
        gameFolder.ID
      );
      return result;
    })
  );

  return parsedFiles;
};

export default FolderParser;
