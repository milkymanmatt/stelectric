import FileSections from 'data_types/FileSections';
import CountrySnapshot from '../../data_types/CountrySnapshot';

const getEmptyCountrySnapshot = (): CountrySnapshot => {
  return {
    name: '',
    overall: {
      military_power: 0,
      economy_power: 0,
      victory_rank: 0,
      victory_score: 0,
      tech_power: 0,
      immigration: 0,
      emigration: 0,
      fleet_size: 0,
      empire_size: 0,
      sapient: 0,
    },
    economy: {
      energy: {
        available: 0,
        income: 0,
        balance: 0,
      },
      minerals: {
        available: 0,
        income: 0,
        balance: 0,
      },
      food: {
        available: 0,
        income: 0,
        balance: 0,
      },
      consumer_goods: {
        available: 0,
        income: 0,
        balance: 0,
      },
      alloys: {
        available: 0,
        income: 0,
        balance: 0,
      },
      influence: {
        available: 0,
        income: 0,
        balance: 0,
      },
      unity: {
        available: 0,
        income: 0,
        balance: 0,
      },
      volatile_motes: {
        available: 0,
        income: 0,
        balance: 0,
      },
      exotic_gases: {
        available: 0,
        income: 0,
        balance: 0,
      },
      rare_crystals: {
        available: 0,
        income: 0,
        balance: 0,
      },
      sr_dark_matter: {
        available: 0,
        income: 0,
        balance: 0,
      },
      nanites: {
        available: 0,
        income: 0,
        balance: 0,
      },
      minor_artifacts: {
        available: 0,
      },
    },
    population: { populationCount: 0 },
    military: {
      starbaseCount: 0,
      starbaseCapacity: 0,
      militaryShipCount: 0,
      corvetteCount: 0,
      destroyerCount: 0,
      cruiserCount: 0,
      battleshipCount: 0,
      fleetMilitaryPower: 0,
    },
    technology: {
      researchedTechCount: 0,
      phyIncome: 0,
      socIncome: 0,
      engIncome: 0,
      storedPhyCount: 0,
      storedSocCount: 0,
      storedEngCount: 0,
    },
    planets: {
      planetCount: 0,
      controlledPlanetCount: 0,
      systemCount: 0,
      colonyShipCount: 0,
      constructionShipCount: 0,
      scienceShipCount: 0,
    },
    traditions: { traditionCount: 0, treesUnlocked: 0, ascPerkCount: 0 },
    epics: { megastructuresCount: 0, relicCount: 0 },
  };
};

export const getEmptyFileSections = (): FileSections => {
  return {
    date: '',
    country: '',
    ships: '',
  };
};

export default getEmptyCountrySnapshot;
