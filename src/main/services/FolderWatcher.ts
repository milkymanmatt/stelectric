// eslint-disable-next-line import/no-cycle
import { SendToRenderer } from '../main';
import FolderExporter from './FolderExporter';
import FolderParser from './FolderParser';

let watchID: NodeJS.Timer;

const FolderWatcher = async (folderpath: string) => {
  if (watchID) {
    clearInterval(watchID);
  }
  await FolderParser(folderpath);
  let snaps = await FolderExporter(folderpath);
  SendToRenderer('gamedata', snaps);

  watchID = setInterval(async () => {
    await FolderParser(folderpath);
    snaps = await FolderExporter(folderpath);
    SendToRenderer('gamedata', snaps);
  }, 60000);
};

export default FolderWatcher;
