import jetpack from 'fs-jetpack';

const FileMaker = async (
  filename: string,
  fileContents: string,
  location?: string,
  consoleLog?: boolean
): Promise<boolean> => {
  if (!filename) {
    throw new Error('FileMaker filename is empty');
  }
  if (!fileContents) {
    throw new Error('FileMaker fileContents is empty');
  }

  let filepath = `./output/${filename}`;
  if (location) {
    filepath = location + filename;
  }

  if (consoleLog) {
    console.log(`outputing file to ${filepath}`);
  }

  try {
    jetpack.file(filepath, { content: fileContents });
  } catch (err) {
    return false;
  }

  return true;
};

export default FileMaker;
