import FileSections from '../../data_types/FileSections';
import CountryParser from './parsers/CountryParser';
import log from './Logger';
import FileSnapshot from '../../data_types/FileSnapshot';
import Entry from '../../data_types/Entry';
import UpdateSaveGame from '../data/UpdateSaveGame';

const FileParser = async (
  folderpath: string,
  file: Entry,
  filename: string,
  fileChopper: (
    fileContents: string,
    filename: string
  ) => Promise<FileSections>,
  gameFolderID: number
): Promise<FileSnapshot> => {
  if (file.uncompressedSize <= 10240) {
    log(`FileParser| filesize is too small to be of use for file ${filename}.`);
    throw new Error(`filesize is too small to be of use for file ${filename}.`);
  }

  if (file.fileName !== 'gamestate') {
    log(
      `FileParser| file is incorrectly named. Could not find gamestate for file ${filename}.`
    );
    throw new Error(
      `file is incorrectly named. Could not find gamestate for file ${filename}.`
    );
  }

  if (!file.zipFile) {
    log(`FileParser| file is missing zipFile property for file ${filename}.`);
    throw new Error(`file is missing zipFile property for file ${filename}.`);
  }

  try {
    log(`FileParser| Attempt to read file contents for file ${filename}.`);
    const chunks: string[] = [];
    let gamestateContent = 'default';

    const fileStream = await file.zipFile.openReadStream(file);

    fileStream.on('data', (chunk) => {
      chunks.push(chunk.toString());
    });

    gamestateContent = await new Promise((resolve) =>
      fileStream.on('end', () => {
        resolve(chunks.join(''));
      })
    );

    log(
      `FileParser| gamestate file contents read for file ${filename}. Attempting to chop gamestate into sections.`
    );

    const parts: FileSections = await fileChopper(gamestateContent, filename);

    // Check for empties
    if (parts.date === '' || parts.country === '' || parts.ships === '') {
      log(`FileParser| fileChopper returned empty sections.`);
    }

    const cSnaps = await CountryParser(parts.country, filename);

    const result: FileSnapshot = {
      date: parts.date,
      countrySnapshots: cSnaps,
    };

    // Save result to DB
    const dbresult = await UpdateSaveGame(
      folderpath,
      filename,
      parts.date,
      result,
      gameFolderID
    );
    if (!dbresult) {
      log(
        `FileParser| Failed to save UpdateSaveGame after recieveing full snapshot object.`
      );
      throw new Error(
        `FileParser failed to save finished entity to the database`
      );
    }

    return result;
  } catch (err: Error | any) {
    log(`FileParser| Encountered Error:`);
    log(err.message);
    throw err;
  }
};

export default FileParser;
