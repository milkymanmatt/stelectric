import Military from '../../../data_types/Military';
import getEmptyCountrySnapshot from '../EmptyGenerators';
import log from '../Logger';
import { cleanNumberFromString } from './EcoHelpers';

const MilitaryParser = (input: string): Military => {
  const result = getEmptyCountrySnapshot().military;

  if (!input || input.length < 24) {
    log(
      'MilitaryParser input is empty or too small to be useable. Returning empty Military object.'
    );
    return result;
  }

  const lines = input.replace(/\t/g, '').replace(/\r/g, '').split('\n');

  for (let i = 0; i < lines.length; i++) {
    if (lines[i].indexOf('num_upgraded_starbase') !== -1) {
      result.starbaseCount = cleanNumberFromString(lines[i]);
    }
    if (lines[i].indexOf('starbase_capacity') !== -1) {
      result.starbaseCapacity = cleanNumberFromString(lines[i]);
    }
  }

  return result;
};

export default MilitaryParser;
