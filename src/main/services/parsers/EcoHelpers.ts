import Resource from '../../../data_types/Resource';
import Economy from '../../../data_types/Economy';

export const getEmptyEconomy = (): Economy => {
  return {
    energy: {
      available: 0,
      income: 0,
      balance: 0,
    },
    minerals: {
      available: 0,
      income: 0,
      balance: 0,
    },
    food: {
      available: 0,
      income: 0,
      balance: 0,
    },
    consumer_goods: {
      available: 0,
      income: 0,
      balance: 0,
    },
    alloys: {
      available: 0,
      income: 0,
      balance: 0,
    },
    influence: {
      available: 0,
      income: 0,
      balance: 0,
    },
    unity: {
      available: 0,
      income: 0,
      balance: 0,
    },
    volatile_motes: {
      available: 0,
      income: 0,
      balance: 0,
    },
    exotic_gases: {
      available: 0,
      income: 0,
      balance: 0,
    },
    rare_crystals: {
      available: 0,
      income: 0,
      balance: 0,
    },
    sr_dark_matter: {
      available: 0,
      income: 0,
      balance: 0,
    },
    nanites: {
      available: 0,
      income: 0,
      balance: 0,
    },
    minor_artifacts: {
      available: 0,
    },
  };
};

export const cleanNumberFromString = (input: string) => {
  if (!input || input.length < 1) {
    return -1;
  }
  const nums = input.replace(/\w+=/g, '').trim();
  if (nums.length) {
    return parseFloat(nums);
  }
  return -1;
};

export const getResourceFromString = (input: string): Resource => {
  if (!input || input.length < 1) {
    return null;
  }
  if (input.indexOf('energy') !== -1) {
    return 'energy';
  }
  if (input.indexOf('minerals') !== -1) {
    return 'minerals';
  }
  if (input.indexOf('food') !== -1) {
    return 'food';
  }
  if (input.indexOf('influence') !== -1) {
    return 'influence';
  }
  if (input.indexOf('unity') !== -1) {
    return 'unity';
  }
  if (input.indexOf('consumer_goods') !== -1) {
    return 'consumer_goods';
  }
  if (input.indexOf('alloys') !== -1) {
    return 'alloys';
  }
  if (input.indexOf('volatile_motes') !== -1) {
    return 'volatile_motes';
  }
  if (input.indexOf('exotic_gases') !== -1) {
    return 'exotic_gases';
  }
  if (input.indexOf('rare_crystals') !== -1) {
    return 'rare_crystals';
  }
  if (input.indexOf('sr_dark_matter') !== -1) {
    return 'sr_dark_matter';
  }
  if (input.indexOf('nanites') !== -1) {
    return 'nanites';
  }
  if (input.indexOf('minor_artifacts') !== -1) {
    return 'minor_artifacts';
  }
  return null;
};
