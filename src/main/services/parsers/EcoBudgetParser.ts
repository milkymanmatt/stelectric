import Economy from '../../../data_types/Economy';
import {
  cleanNumberFromString,
  getEmptyEconomy,
  getResourceFromString,
} from './EcoHelpers';

const EcoBudgetParser = (input: string): Economy => {
  if (!input || input.length < 1024) {
    throw new Error(
      'EcoBudgetParser input is empty or too small to be useable'
    );
  }

  const result: Economy = getEmptyEconomy();

  // Handle Incomes
  const incomeTotals = {
    energy: 0,
    minerals: 0,
    food: 0,
    influence: 0,
    unity: 0,
    consumer_goods: 0,
    alloys: 0,
    volatile_motes: 0,
    exotic_gases: 0,
    rare_crystals: 0,
    sr_dark_matter: 0,
    nanites: 0,
    minor_artifacts: 0,
  };

  const incomeObj = input.match(/income={$([\S\s]+?)(^\t\t\t\t}$)/gm);

  if (!incomeObj || incomeObj.length < 1) {
    throw new Error('EcoBudgetParser could not find a valid income object');
  }

  const incomeArray = incomeObj[0]
    .replace(/\t/g, '')
    .replace(/\r/g, '')
    .split('\n')
    .filter((line) => {
      return line.indexOf('{') === -1 && line.indexOf('}') === -1;
    });

  for (let i = 0; i < incomeArray.length; i++) {
    const resource = getResourceFromString(incomeArray[i]);
    if (resource) {
      incomeTotals[resource] += cleanNumberFromString(incomeArray[i]);
    }
  }

  result.energy.income = incomeTotals.energy;
  result.minerals.income = incomeTotals.minerals;
  result.food.income = incomeTotals.food;
  result.influence.income = incomeTotals.influence;
  result.unity.income = incomeTotals.unity;
  result.consumer_goods.income = incomeTotals.consumer_goods;
  result.alloys.income = incomeTotals.alloys;
  result.volatile_motes.income = incomeTotals.volatile_motes;
  result.exotic_gases.income = incomeTotals.exotic_gases;
  result.rare_crystals.income = incomeTotals.rare_crystals;
  result.sr_dark_matter.income = incomeTotals.sr_dark_matter;
  result.nanites.income = incomeTotals.nanites;

  // Handle Balance
  const balanceTotals = {
    energy: 0,
    minerals: 0,
    food: 0,
    influence: 0,
    unity: 0,
    consumer_goods: 0,
    alloys: 0,
    volatile_motes: 0,
    exotic_gases: 0,
    rare_crystals: 0,
    sr_dark_matter: 0,
    nanites: 0,
    minor_artifacts: 0,
  };

  const balanceObj = input.match(/balance={$([\S\s]+?)(^\t\t\t\t}$)/gm);

  if (!balanceObj || balanceObj.length < 1) {
    throw new Error('EcoBudgetParser could not find a valid balance object');
  }

  const balanceArray = balanceObj[0]
    .replace(/\t/g, '')
    .replace(/\r/g, '')
    .split('\n')
    .filter((line) => {
      return line.indexOf('{') === -1 && line.indexOf('}') === -1;
    });

  for (let i = 0; i < balanceArray.length; i++) {
    const resource = getResourceFromString(balanceArray[i]);
    if (resource) {
      balanceTotals[resource] += cleanNumberFromString(balanceArray[i]);
    }
  }

  result.energy.balance = balanceTotals.energy;
  result.minerals.balance = balanceTotals.minerals;
  result.food.balance = balanceTotals.food;
  result.influence.balance = balanceTotals.influence;
  result.unity.balance = balanceTotals.unity;
  result.consumer_goods.balance = balanceTotals.consumer_goods;
  result.alloys.balance = balanceTotals.alloys;
  result.volatile_motes.balance = balanceTotals.volatile_motes;
  result.exotic_gases.balance = balanceTotals.exotic_gases;
  result.rare_crystals.balance = balanceTotals.rare_crystals;
  result.sr_dark_matter.balance = balanceTotals.sr_dark_matter;
  result.nanites.balance = balanceTotals.nanites;

  return result;
};

export default EcoBudgetParser;
