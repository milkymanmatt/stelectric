/* eslint-disable prefer-destructuring */
import Technology from '../../../data_types/Technology';
import getEmptyCountrySnapshot from '../EmptyGenerators';
import log from '../Logger';

const TechnologyParser = (input: string): Technology => {
  const result = getEmptyCountrySnapshot().technology;

  if (!input || input.length < 512) {
    log(
      'TechnologyParser input is empty or too small to be useable. Returning empty Technology object.'
    );
    return result;
  }

  const getResearchedCount = () => {
    const completedTechs = input.match(/(tech_status={$)(.|\s)+?(\t\tp)/gm);
    if (!completedTechs || completedTechs.length < 1) {
      return -1;
    }

    const levels = completedTechs[0]
      .replace(/\r/g, '')
      .split('\n')
      .filter((line) => {
        return line.length < 12;
      });

    const numbers = levels.map((level) => {
      const num = parseInt(level.replace('level=', '').trim(), 10);
      return Number.isNaN(num) ? 0 : num;
    });

    return numbers.reduce((a, b) => a + b);
  };
  result.researchedTechCount = getResearchedCount();

  const getStoredResearchTechpoints = () => {
    const storedTechpoints = input.match(/stored_techpoints={\s+([\d ]+)\s+}/m);
    if (!storedTechpoints || storedTechpoints.length < 2) {
      return [-1, -1, -1];
    }

    return storedTechpoints[1]
      .trim()
      .split(' ')
      .map((str) => parseInt(str, 10));
  };

  const stored = getStoredResearchTechpoints();
  result.storedPhyCount = stored[0];
  result.storedSocCount = stored[1];
  result.storedEngCount = stored[2];

  return result;
};

export default TechnologyParser;
