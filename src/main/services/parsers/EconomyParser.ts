import Economy from '../../../data_types/Economy';
import getEmptyCountrySnapshot from '../EmptyGenerators';
import log from '../Logger';
import EcoBudgetParser from './EcoBudgetParser';
import { cleanNumberFromString, getResourceFromString } from './EcoHelpers';

const EconomyParser = (
  budgetInput: string,
  availableInput: string
): Economy => {
  if (!budgetInput || budgetInput.length < 512) {
    log(
      `EconomyParser| budgetInput is empty or too small to be useable. Returning empty Economy object.`
    );
    return getEmptyCountrySnapshot().economy;
  }

  if (!availableInput || availableInput.length < 64) {
    log(
      'EconomyParser| availableInput is empty or too small to be useable. Returning empty Economy object.'
    );
    return getEmptyCountrySnapshot().economy;
  }

  // Handle Incomes & Balances
  const result = EcoBudgetParser(budgetInput);

  // Set "Available" numbers
  const availArray = availableInput
    .replace(/\t/g, '')
    .replace(/\r/g, '')
    .split('\n')
    .filter((line) => {
      return line.indexOf('{') === -1 && line.indexOf('}') === -1;
    });

  for (let i = 0; i < availArray.length; i++) {
    const resource = getResourceFromString(availArray[i]);
    if (resource) {
      result[resource].available = cleanNumberFromString(availArray[i]);
    }
  }

  return result;
};

export default EconomyParser;
