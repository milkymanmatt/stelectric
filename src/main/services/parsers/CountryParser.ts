/* eslint-disable no-restricted-globals */
import CountrySnapshot from '../../../data_types/CountrySnapshot';
import FileMaker from '../FileMaker';
import log from '../Logger';
import EconomyParser from './EconomyParser';
import EpicParser from './EpicParser';
import MilitaryParser from './MilitaryParser';
import OverallParser from './OverallParser';
import PlanetParser from './PlanetParser';
import TechnologyParser from './TechnologyParser';
import TraditionParser from './TraditionParser';

const CountryParser = async (
  unparsedInput: string,
  filename: string,
  savePartsToFile = false
) => {
  if (!unparsedInput || unparsedInput.length < 1024) {
    log(
      `CountryParser| input recieved is empty or too small to be of use for file ${filename}.`
    );
    log('### ### ### ### ###');
    log(unparsedInput);
    log('### ### ### ### ###\r\n');
    throw new Error(
      `CountryParser| input recieved is empty or too small to be of use.`
    );
  }

  const validateRegexMatch = (input: string, reg: RegExp, errMsg: string) => {
    const regResult = input.match(reg);
    if (!regResult || !regResult.length) {
      log(`CountryParser| validateRegexMatch failed.`);
      log(`${errMsg}`);
      return '';
    }
    return regResult[0];
  };

  const countries = unparsedInput.match(/(^\t\d={$)([\S\s]+?)(^\t}$)/gm);

  if (countries?.length) {
    const files = [];
    const countrySnapshots: CountrySnapshot[] = [];
    for (let i = 0; i < countries?.length; i++) {
      // Country Name
      const countryNameReg = countries[i].match(/name="(.+?)"/);
      const countryName = countryNameReg?.length
        ? countryNameReg[1]
        : 'Unknown';

      // Overall
      const overall = OverallParser(
        validateRegexMatch(
          countries[i],
          /military_power=([\w\s\-_.=]+?)graphical_culture/gm,
          `CountryParser| could not parse overall object for ${countryName}`
        )
      );

      // Economy
      const allEcoBudgetObj = validateRegexMatch(
        countries[i],
        /current_month={$([\w\s=_{}.-]+?)^\t\t\t}$/gm,
        `CountryParser could not parse budget economy object for ${countryName}`
      );
      const allEcoAvailObj = validateRegexMatch(
        countries[i],
        /standard_economy_module={$([\S\s]+?)(^\t\t\t}$)/gm,
        `CountryParser could not parse available economy object for ${countryName}`
      );
      const economy = EconomyParser(allEcoBudgetObj, allEcoAvailObj);

      // Population
      const allPop = countries[i].match(/employable_pops=([\d]+?)$/m);
      let popCount = 0;
      if (allPop && allPop.length > 0) {
        popCount = parseInt(allPop[1], 10);
      }

      // Military
      const military = MilitaryParser(
        validateRegexMatch(
          countries[i],
          /num_upgraded_starbase=([\w\s_.=-]+?)employable_pops/m,
          `CountryParser could not parse military object for ${countryName}`
        )
      );

      // Technology
      const technology = TechnologyParser(
        validateRegexMatch(
          countries[i],
          /(tech_status={$)([\S\s])+?(^\t\t}$)/gm,
          `CountryParser could not parse technology object for ${countryName}`
        )
      );

      // Planets
      const planets = PlanetParser(
        validateRegexMatch(
          countries[i],
          /owned_planets=([\w\s_={}]+?)ship_design_collection/gm,
          `CountryParser could not parse planet object for ${countryName}`
        )
      );

      // Traditions
      const traditions = TraditionParser(
        validateRegexMatch(
          countries[i],
          /tradition_categories=([\w\s_={}"]+?)owned_armies/gm,
          `CountryParser could not parse traditions object for ${countryName}`
        )
      );

      // Epics
      const megaObj =
        countries[i].match(/owned_megastructures={$([\w\s]+?)}/m) || '';
      const relicObj = countries[i].match(/relics={$([\w\s"]+?)}/m) || '';
      const epics = EpicParser(megaObj[0], relicObj[0]);

      countrySnapshots[i] = {
        name: countryName,
        overall,
        economy,
        population: {
          populationCount: popCount,
        },
        military,
        technology,
        planets,
        traditions,
        epics,
      };

      if (savePartsToFile) {
        files.push(FileMaker(`country_${i}.txt`, countries[i]));
      }
    }
    if (savePartsToFile) {
      await Promise.all(files);
    }
    return countrySnapshots;
  }

  return [];
};

export default CountryParser;
