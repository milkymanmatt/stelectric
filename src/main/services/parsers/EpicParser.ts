import Epics from '../../../data_types/Epics';
import getEmptyCountrySnapshot from '../EmptyGenerators';
import log from '../Logger';

const EpicParser = (megaInput: string, relicInput: string): Epics => {
  const result = getEmptyCountrySnapshot().epics;

  if (!megaInput || megaInput.length < 8) {
    result.megastructuresCount = 0;
    log(
      'EpicParser| megaInput is empty or too small to be useable. Setting to zero'
    );
  } else {
    const mega = megaInput
      .replace(/[\t\r]/g, '')
      .split(' ')
      .filter((el) => {
        return el.length > 0;
      }).length;
    result.megastructuresCount = mega;
  }

  if (!relicInput || relicInput.length < 8) {
    result.relicCount = 0;
    log(
      'EpicParser| relicInput is empty or too small to be useable. Setting to zero'
    );
  } else {
    const relics = relicInput
      .replace(/[\t\r]/g, '')
      .split('\n')
      .filter((el) => {
        return el.length > 0;
      }).length;

    result.relicCount = relics;
  }

  return result;
};

export default EpicParser;
