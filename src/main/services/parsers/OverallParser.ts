import Overall from '../../../data_types/Overall';
import getEmptyCountrySnapshot from '../EmptyGenerators';
import { cleanNumberFromString } from './EcoHelpers';

const OverallParser = (input: string): Overall => {
  if (!input || input.length < 64) {
    throw new Error('OverallParser input is empty or too small to be useable');
  }

  const result = getEmptyCountrySnapshot().overall;

  const lines = input.replace(/\t/g, '').replace(/\r/g, '').split('\n');

  for (let i = 0; i < lines.length; i++) {
    if (lines[i].indexOf('military_power') !== -1) {
      result.military_power = cleanNumberFromString(lines[i]);
    }
    if (lines[i].indexOf('economy_power') !== -1) {
      result.economy_power = cleanNumberFromString(lines[i]);
    }
    if (lines[i].indexOf('victory_rank') !== -1) {
      result.victory_rank = cleanNumberFromString(lines[i]);
    }
    if (lines[i].indexOf('victory_score') !== -1) {
      result.victory_score = cleanNumberFromString(lines[i]);
    }
    if (lines[i].indexOf('tech_power') !== -1) {
      result.tech_power = cleanNumberFromString(lines[i]);
    }
    if (lines[i].indexOf('immigration') !== -1) {
      result.immigration = cleanNumberFromString(lines[i]);
    }
    if (lines[i].indexOf('emigration') !== -1) {
      result.emigration = cleanNumberFromString(lines[i]);
    }
    if (lines[i].indexOf('fleet_size') !== -1) {
      result.fleet_size = cleanNumberFromString(lines[i]);
    }
    if (lines[i].indexOf('empire_size') !== -1) {
      result.empire_size = cleanNumberFromString(lines[i]);
    }
    if (lines[i].indexOf('sapient') !== -1) {
      result.sapient = cleanNumberFromString(lines[i]);
    }
  }

  return result;
};

export default OverallParser;
