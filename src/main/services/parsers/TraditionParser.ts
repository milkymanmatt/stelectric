import Traditions from '../../../data_types/Traditions';
import getEmptyCountrySnapshot from '../EmptyGenerators';
import log from '../Logger';

const TraditionParser = (input: string): Traditions => {
  const result = getEmptyCountrySnapshot().traditions;

  if (!input || input.length < 32) {
    log(
      'TraditionParser input is empty or too small to be useable. returning empty Traditions object.'
    );
    return result;
  }

  // traditionCount
  result.traditionCount = 0;
  const traditionsObject = input.match(/traditions={([\w\s_"]+?)}$/m);
  if (traditionsObject && traditionsObject.length > 1) {
    const numTraditions = traditionsObject[1]
      .replace(/[\t\r]/g, '')
      .split('\n')
      .filter((el) => {
        return el.length > 0;
      }).length;

    result.traditionCount = numTraditions;
  }

  // treesUnlocked
  const tradTreesObject = input.match(/tradition_categories={([\w\s_"]+?)}$/m);
  if (!tradTreesObject || tradTreesObject.length < 2) {
    log('TraditionParser could not find tradition_categories object');
    result.treesUnlocked = 0;
  } else {
    const numTrees = tradTreesObject[1]
      .replace(/[\t\r]/g, '')
      .split('\n')
      .filter((el) => {
        return el.length > 0;
      }).length;

    result.treesUnlocked = numTrees;
  }

  // ascension_perks
  const ascPerksObject = input.match(/ascension_perks={([\w\s_"]+?)}$/m);
  if (!ascPerksObject || ascPerksObject.length < 2) {
    log(
      'TraditionParser could not find any ascension_perks object, setting to zero'
    );
    result.ascPerkCount = 0;
  } else {
    const numAscPerks = ascPerksObject[1]
      .replace(/[\t\r]/g, '')
      .split('\n')
      .filter((el) => {
        return el.length > 0;
      });
    result.ascPerkCount = numAscPerks.length;
  }

  return result;
};

export default TraditionParser;
