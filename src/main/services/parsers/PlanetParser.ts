import Planet from '../../../data_types/Planet';
import getEmptyCountrySnapshot from '../EmptyGenerators';
import log from '../Logger';

const PlanetParser = (input: string): Planet => {
  const result = getEmptyCountrySnapshot().planets;

  if (!input || input.length < 32) {
    log(
      'PlanetParser input is empty or too small to be useable. Returning empty Planet object.'
    );
    return result;
  }

  // Split the input into owned_planets and controlled_planets
  const ownedPlanetsObj = input.match(/owned_planets={([\w\s_={}]+?)}$/m);
  if (!ownedPlanetsObj || ownedPlanetsObj.length < 2) {
    throw new Error('PlanetParser could not find owned_planets object');
  }

  const numOwnedPlanets = ownedPlanetsObj[1].split(' ').length;

  result.planetCount = numOwnedPlanets;

  // controlled_planets
  const contPlanetsObj = input.match(/controlled_planets={([\w\s_={}]+?)}$/m);
  if (!contPlanetsObj || contPlanetsObj.length < 2) {
    throw new Error('PlanetParser could not find controlled_planets object');
  }

  const numContPlanets = contPlanetsObj[1].split(' ').length;

  result.controlledPlanetCount = numContPlanets;

  return result;
};

export default PlanetParser;
