import GameFolderDTO from '../../data_types/GameFolderDTO';
import DatabaseConnection from '../data/DatabaseConnection';
import SaveGameDTO from '../../data_types/SaveGameDTO';
import log from './Logger';

const FolderExporter = async (folderpath: string) => {
  try {
    const gf = await (
      await DatabaseConnection()
    ).get<GameFolderDTO>(`SELECT * FROM GAMEFOLDER WHERE Folderpath = :fp`, {
      ':fp': folderpath,
    });

    if (!gf) {
      return '';
    }

    const allGames = await (
      await DatabaseConnection()
    ).all<SaveGameDTO[]>(
      `SELECT * FROM SAVEGAME WHERE GameFolderID = ?`,
      gf.ID
    );

    const allSnapsString = allGames
      .map((game) => {
        return JSON.stringify(game.SnapshotObj);
      })
      .join('\r\n')
      .replace(/\\"/g, '"')
      .replace(/"\r\n"/g, ',\r\n');

    const allSnaps = `[${allSnapsString.substring(
      1,
      allSnapsString.length - 1
    )}]`;

    return allSnaps;
  } catch (err: any) {
    log(`FolderExporter| Error: ${err.message}`);
    return '';
  }
};

export default FolderExporter;
