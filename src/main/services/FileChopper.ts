import FileSections from '../../data_types/FileSections';
import { getEmptyFileSections } from './EmptyGenerators';
import FileMaker from './FileMaker';
import log from './Logger';

/* eslint-disable @typescript-eslint/no-unused-vars */
export type DisplaySections = {
  economy: string;
  population: string;
  military: string;
  technology: string;
  planets: string;
  traditions: string;
  epics: string;
};

const FileChopper = async (
  fileContents: string,
  fileName: string
): Promise<FileSections> => {
  if (!fileContents) {
    throw new Error('fileContents is empty');
  }
  const ChopSection = async (
    regex: RegExp,
    outputToFile: boolean,
    outputFilename?: string
  ) => {
    const section = fileContents.match(regex);

    if (!section || !section[0]) {
      log(
        `FileChopper|ChopSection| no matching object could be located for: ${regex}`
      );
      return [];
    }

    if (outputToFile) {
      if (!outputFilename) {
        // eslint-disable-next-line no-param-reassign
        outputFilename = 'section.txt';
      }
      const madeFile = await FileMaker(
        outputFilename,
        section[0],
        undefined,
        false
      );

      if (!madeFile) {
        log(`FileChopper|ChopSection| file creation failed`);
      }
    }

    return section;
  };

  try {
    const date = (
      await ChopSection(/^date="([\d.]+)"$/m, true, 'date.txt')
    )[1] as string;

    const country = await ChopSection(
      /^country={([\s\S]+?)^}$/m,
      true,
      `${fileName}_country.txt`
    );

    const ships = 'test';
    // await ChopSection(/^ships={$(.|[\s])+?^}$/m, true, 'ships.txt');

    const result: FileSections = {
      date,
      country: country[1],
      ships: ships[0],
    };

    return result;
  } catch (err) {
    log(
      `FileChopper| generic error, likely unable to locate regex match. Returning empty object. Info: ${err}`
    );
    return getEmptyFileSections();
  }
};

export default FileChopper;
