import { Navigation } from './data_types/NavigationContracts';

export {};
declare global {
  interface Window {
    ipc_api: {
      ipcRenderer: {
        loadNav: () => Promise<Navigation>;
        log: (toLog: string) => void;
        send: (channel: string, ...arg: any[]) => void;
        on: (channel: string, func: any) => void;
        once: (channel: string, func: any) => void;
      };
    };
  }
}
