import { Provider } from 'react-redux';
import './App.css';
import MainFrame from './components/Mainframe/Mainframe';
import store from './store';

const App = () => {
  return (
    <Provider store={store}>
      <div>
        <MainFrame />
      </div>
    </Provider>
  );
};

export default App;
