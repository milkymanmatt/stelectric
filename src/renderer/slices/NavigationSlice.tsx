import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Navigation } from '../../data_types/NavigationContracts';

export type SelectedNav = {
  name: string;
  id: string;
  index: number;
};

export interface LoadedState {
  isNavLoaded: boolean;
  nav: Navigation;
  selectedNav: SelectedNav;
  selectedSideNav: SelectedNav;
}
const initialState: LoadedState = {
  isNavLoaded: false,
  nav: {
    buttons: [],
  },
  selectedNav: { name: '', id: '', index: -1 },
  selectedSideNav: { name: '', id: '', index: -1 },
};
export const NavigationSlice = createSlice({
  name: 'navigation',
  initialState,
  reducers: {
    setIsNavLoaded: (state, action: PayloadAction<boolean>) => {
      state.isNavLoaded = action.payload;
    },
    setNav: (state, action: PayloadAction<Navigation>) => {
      state.nav = action.payload;
    },
    setSelectedNav: (state, action: PayloadAction<SelectedNav>) => {
      state.selectedNav = action.payload;
    },
    setSelectedSideNav: (state, action: PayloadAction<SelectedNav>) => {
      state.selectedSideNav = action.payload;
    },
  },
});

export const { setIsNavLoaded, setNav, setSelectedNav, setSelectedSideNav } =
  NavigationSlice.actions;

export default NavigationSlice.reducer;
