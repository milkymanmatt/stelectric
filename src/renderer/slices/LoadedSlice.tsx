import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import FileSnapshot from '../../data_types/FileSnapshot';

export interface LoadedState {
  isLoaded: boolean;
  folderpath: string;
  decodedData: FileSnapshot[];
}
const initialState: LoadedState = {
  isLoaded: false,
  folderpath: '',
  decodedData: [],
};
export const LoadedSlice = createSlice({
  name: 'loaded',
  initialState,
  reducers: {
    setIsLoaded: (state, action: PayloadAction<boolean>) => {
      state.isLoaded = action.payload;
    },
    setFolderpath: (state, action: PayloadAction<string>) => {
      state.folderpath = action.payload;
    },
    setDecodedData: (state, action: PayloadAction<FileSnapshot[]>) => {
      state.decodedData = action.payload;
    },
  },
});

export const { setIsLoaded, setFolderpath, setDecodedData } =
  LoadedSlice.actions;

export default LoadedSlice.reducer;
