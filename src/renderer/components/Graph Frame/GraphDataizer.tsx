import { useAppSelector } from '../../store';

export type CountryDataset = {
  name: string;
  dates: string[];
  values: number[];
};

const GraphDataizer = () => {
  const data = useAppSelector((allstate) => allstate.loaded.decodedData);

  const navState = useAppSelector((allstate) => allstate.nav);
  const selNav = navState.selectedNav;
  const selSideNav = navState.selectedSideNav;

  let results: CountryDataset[] = [];

  if (navState.selectedNav.id && navState.selectedSideNav.id) {
    for (let d = 0; d < data.length; d++) {
      const { date } = data[d];
      for (let c = 0; c < data[d].countrySnapshots.length; c++) {
        // get value
        let value: number;

        // There may be dots
        const parts = selSideNav.id.split('.');
        if (parts.length > 1) {
          // Goodbye type saftey
          value = (data[d].countrySnapshots[c] as any)[selNav.id][parts[0]][
            parts[1]
          ];
        } else {
          // Goodbye type saftey
          value = (data[d].countrySnapshots[c] as any)[selNav.id][
            selSideNav.id
          ];
        }

        const countryExists = results.some(
          (obj) => obj.name === data[d].countrySnapshots[c].name
        );
        if (!countryExists) {
          results.push({
            name: data[d].countrySnapshots[c].name,
            dates: [date],
            values: [value],
          });
        } else {
          // insertion sort
          results = results.map((cd) => {
            if (cd.name === data[d].countrySnapshots[c].name) {
              const newDateParts = date.split('.');
              for (let srt = 0; srt < cd.dates.length; srt++) {
                const comparisonDateParts = cd.dates[srt].split('.');
                // compare year
                if (
                  parseInt(newDateParts[0], 10) <
                  parseInt(comparisonDateParts[0], 10)
                ) {
                  cd.dates.splice(srt, 0, date);
                  cd.values.splice(srt, 0, value);
                  break;
                }
                if (
                  parseInt(newDateParts[0], 10) >
                  parseInt(comparisonDateParts[0], 10)
                ) {
                  if (srt === cd.dates.length - 1) {
                    cd.dates.push(date);
                    cd.values.push(value);
                    break;
                  }
                  continue;
                }

                // compare months
                if (
                  parseInt(newDateParts[1], 10) <
                  parseInt(comparisonDateParts[1], 10)
                ) {
                  cd.dates.splice(srt, 0, date);
                  cd.values.splice(srt, 0, value);
                  break;
                }
                if (
                  parseInt(newDateParts[1], 10) >
                  parseInt(comparisonDateParts[1], 10)
                ) {
                  if (srt === cd.dates.length - 1) {
                    cd.dates.push(date);
                    cd.values.push(value);
                    break;
                  }
                  continue;
                }

                // compare days
                if (
                  parseInt(newDateParts[2], 10) <
                  parseInt(comparisonDateParts[2], 10)
                ) {
                  cd.dates.splice(srt, 0, date);
                  cd.values.splice(srt, 0, value);
                  break;
                }
                cd.dates.push(date);
                cd.values.push(value);
              }
            }
            return cd;
          });
        }
      }
    }
  }

  return results;
};

export default GraphDataizer;
