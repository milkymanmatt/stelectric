/* eslint-disable react/destructuring-assignment */
import { useRef } from 'react';
import Plot from 'react-plotly.js';
import { useAppSelector } from '../../store';
import GraphDataizer from './GraphDataizer';

const countryColours = [
  'rgb(240,128,128)',
  'rgb(138,43,226)',
  'rgb(0,0,128)',
  'rgb(139,0,0)',
  'rgb(0,128,0)',
  'rgb(255,246,143)',
  'rgb(218,165,32)',
  'rgb(32,178,170)',
  'rgb(122,255,160)',
  'rgb(255,218,185)',
  'rgb(192,214,228)',
  'rgb(255,0,255)',
  'rgb(255,127,80)',
  'rgb(175,238,238)',
  'rgb(70,132,153)',
  'rgb(203,190,181)',
  'rgb(255,195,160)',
  'rgb(0,206,209)',
  'rgb(180,238,180)',
  'rgb(246,84,106)',
  'rgb(182,252,213)',
  'rgb(102,0,102)',
  'rgb(255,215,0)',
  'rgb(255,165,0)',
  'rgb(255,115,115)',
  'rgb(211,255,206)',
  'rgb(51,153,255)',
  'rgb(204,204,204)',
  'rgb(0,255,255)',
  'rgb(71,71,71)',
  'rgb(255,165,10)',
  'rgb(92,219,92)',
];

const GraphFrame = () => {
  const ref = useRef<HTMLHeadingElement>(null);

  const navState = useAppSelector((allstate) => allstate.nav);
  const isLoaded = useAppSelector((allstate) => allstate.loaded.isLoaded);

  const getTitle = () => {
    return `${navState.selectedNav.name} - ${navState.selectedSideNav.name} `;
  };

  const getData = GraphDataizer();
  const formatData = (): Plotly.Data[] => {
    return getData.map((cd, index) => {
      return {
        x: cd.dates,
        y: cd.values,
        name: cd.name,
        type: 'scatter',
        mode: 'lines+markers',
        marker: { color: countryColours[index] },
      };
    });
  };

  const showInfo = () => {
    return (
      !isLoaded ||
      navState.selectedNav.index === -1 ||
      navState.selectedSideNav.index === -1
    );
  };

  const showInfoMessage = () => {
    if (!isLoaded) {
      return (
        <p>
          Load a Stellaris game folder to get started.
          <br />
          Open a folder in menu above: File -&gt; Open or by pressing ctrl + o
        </p>
      );
    }
    return (
      <p>
        Select a Category from above and a Dataset to the left to see some stats
      </p>
    );
  };

  return (
    <div className="graph-frame" ref={ref}>
      <div className={showInfo() ? 'info active' : 'info'}>
        {showInfoMessage()}
      </div>
      <div className={showInfo() ? 'graph' : 'graph active'}>
        <Plot
          data={formatData()}
          layout={{
            autosize: true,
            font: {
              color: 'rgb(253, 247, 250)',
            },
            paper_bgcolor: 'rgb(26,27,38)',
            plot_bgcolor: 'rgb(26,27,38)',
            title: {
              font: {
                color: 'rgb(253, 247, 250)',
                size: 24,
              },
              text: getTitle(),
            },
            yaxis: { fixedrange: true, gridcolor: 'rgb(26, 47, 60)' },
            xaxis: { fixedrange: true, gridcolor: 'rgb(26, 47, 60)' },
          }}
          config={{
            displaylogo: false,
            modeBarButtonsToRemove: [
              'zoom2d',
              'pan2d',
              'select2d',
              'lasso2d',
              'zoomIn2d',
              'zoomOut2d',
              'autoScale2d',
              'resetScale2d',
              'zoom3d',
              'pan3d',
              'orbitRotation',
              'tableRotation',
              'resetCameraDefault3d',
              'resetCameraLastSave3d',
              'hoverClosest3d',
              'hoverClosestCartesian',
              'hoverCompareCartesian',
              'zoomInGeo',
              'zoomOutGeo',
              'resetGeo',
              'hoverClosestGeo',
              'hoverClosestGl2d',
              'hoverClosestPie',
              'toggleHover',
              'resetViews',
              // 'toImage',
              'sendDataToCloud',
              'toggleSpikelines',
            ],
            scrollZoom: false,
          }}
          useResizeHandler
          style={{ maxWidth: '100%', height: '100%' }}
        />
      </div>
    </div>
  );
};

export default GraphFrame;
