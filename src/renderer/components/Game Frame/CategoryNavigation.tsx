import { useDispatch } from 'react-redux';
import {
  setSelectedNav,
  setSelectedSideNav,
} from '../../slices/NavigationSlice';
import { useAppSelector } from '../../store';
import './CategoryNavigation.css';

const CategoryNavigation = () => {
  const navState = useAppSelector((allstate) => allstate.nav);
  const isLoaded = useAppSelector((allstate) => allstate.loaded.isLoaded);
  const dispatch = useDispatch();

  const onNavBtnClick = (text: string, valueid: string, index: number) => {
    dispatch(setSelectedSideNav({ name: '', id: '', index: -1 }));
    dispatch(setSelectedNav({ name: text, id: valueid, index }));
  };

  const generateButtons = () => {
    return navState.nav.buttons.map((btn, index) => {
      return (
        <button
          type="button"
          className={btn.valueid === navState.selectedNav.id ? 'active' : ''}
          key={btn.valueid}
          onClick={() => onNavBtnClick(btn.text, btn.valueid, index)}
        >
          {btn.text}
        </button>
      );
    });
  };

  const getClassName = () => {
    if (!isLoaded) {
      return 'category-navigation hidden';
    }
    return 'category-navigation';
  };

  return <div className={getClassName()}>{generateButtons()}</div>;
};

export default CategoryNavigation;
