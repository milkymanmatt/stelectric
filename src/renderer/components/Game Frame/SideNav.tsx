/* eslint-disable consistent-return */
import { useDispatch } from 'react-redux';
import { setSelectedSideNav } from '../../slices/NavigationSlice';
import { useAppSelector } from '../../store';
import './SideNav.css';

const SideNavigation = () => {
  const navState = useAppSelector((allstate) => allstate.nav);
  const dispatch = useDispatch();

  const generateSideButtons = () => {
    const sideBtnsForNav = navState.nav.buttons[navState.selectedNav.index];
    if (!sideBtnsForNav) {
      return;
    }

    const onSideNavBtnClick = (
      text: string,
      valueid: string,
      index: number
    ): any => {
      dispatch(setSelectedSideNav({ name: text, id: valueid, index }));
    };

    return sideBtnsForNav.sideNavButtons.map((btn, index) => {
      return (
        <button
          type="button"
          className={
            btn.valueid === navState.selectedSideNav.id ? 'active' : ''
          }
          key={btn.valueid}
          onClick={() => onSideNavBtnClick(btn.text, btn.valueid, index)}
        >
          {btn.text}
        </button>
      );
    });
  };

  return <div className="side-navigation">{generateSideButtons()}</div>;
};

export default SideNavigation;
