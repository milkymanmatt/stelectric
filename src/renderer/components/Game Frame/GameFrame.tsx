import { useEffect } from 'react';
import GraphFrame from '../Graph Frame/GraphFrame';
import CategoryNavigation from './CategoryNavigation';
import SideNavigation from './SideNav';
import './GameFrame.css';
import store from '../../store';
import { setIsNavLoaded, setNav } from '../../slices/NavigationSlice';

const loadNavigationData = async () => {
  const nav = await window.ipc_api.ipcRenderer.loadNav();
  const { dispatch } = store;
  dispatch(setNav(nav));
  dispatch(setIsNavLoaded(true));
  // Navigation
};

const GameFrame = () => {
  useEffect(() => {
    loadNavigationData();
  }, []);

  return (
    <div className="game-frame">
      <CategoryNavigation />
      <div className="flex">
        <SideNavigation />
        <GraphFrame />
      </div>
    </div>
  );
};

export default GameFrame;
