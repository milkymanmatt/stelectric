import { useAppSelector } from 'renderer/store';
import './HeaderBanner.css';

const HeaderBanner = () => {
  const folderpath = useAppSelector((allstate) => allstate.loaded.folderpath);

  const returnHeaderText = () => {
    let result = null;
    if (folderpath) {
      result = folderpath.match(/([^/\\]*)[/\\]*$/)?.[1];
    }
    return result || 'Load a game to see stats';
  };

  return (
    <div className="header-banner">
      <h1>{returnHeaderText()}</h1>
    </div>
  );
};

export default HeaderBanner;
