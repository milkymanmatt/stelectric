/* eslint-disable react/destructuring-assignment */
import FileSnapshot from 'data_types/FileSnapshot';
import { useDispatch } from 'react-redux';
import {
  setDecodedData,
  setFolderpath,
  setIsLoaded,
} from '../../slices/LoadedSlice';
import HeaderBanner from '../Banner/HeaderBanner';
import GameFrame from '../Game Frame/GameFrame';
import './Mainframe.css';

const MainFrame = () => {
  const dispatch = useDispatch();
  window.ipc_api.ipcRenderer.on('gamedata', (jsonBlob: string) => {
    window.ipc_api.ipcRenderer.log('recieved gamedata');
    if (jsonBlob) {
      const decoded: FileSnapshot[] = JSON.parse(jsonBlob);
      dispatch(setIsLoaded(true));
      dispatch(setDecodedData(decoded));
    }
  });

  window.ipc_api.ipcRenderer.on('selectedFolderMenu', (filepath: string) => {
    window.ipc_api.ipcRenderer.log('recieved selectedFolderMenu');
    if (filepath) {
      dispatch(setFolderpath(filepath));
    }
  });

  return (
    <div className="main-frame">
      <HeaderBanner />
      <GameFrame />
    </div>
  );
};

export default MainFrame;
