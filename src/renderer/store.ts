/* eslint-disable import/no-named-as-default */
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import LoadedSlice from './slices/LoadedSlice';
import NavigationSlice from './slices/NavigationSlice';

const store = configureStore({
  reducer: {
    loaded: LoadedSlice,
    nav: NavigationSlice,
  },
  devTools: process.env.NODE_ENV !== 'production',
});

export default store;

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;

// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;

// Add some hooks
export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
